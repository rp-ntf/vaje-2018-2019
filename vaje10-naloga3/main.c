// Sestavi funkcijo, ki kopira znake iz ene datoteke v drugo.
// Uporabnik poda imeni obeh datotek.
#include <stdio.h>

void KopirajDatoteko(char *vhod, char *izhod)
{
    FILE *vhdat = fopen(vhod,"rt");
    FILE *izhdat = fopen(izhod, "wt");
    
    if((vhdat!=NULL)&&(izhdat!=NULL)) {
       int znak;
        
        while((znak = fgetc(vhdat)) != EOF) {
            fputc(znak,izhdat);
            putchar(znak);
        }
            
        fclose(vhdat);
        fclose(izhdat);
    }
    else printf("Napaka! Ne morem odpreti datoteke.");
}

int main()
{
    char imevhdat[200], imeizhdat[200];
    
	printf("Vpisi ime datoteke za branje: "); 
    gets(imevhdat);
	printf("Vpisi ime datoteke za pisanje: "); 
    gets(imeizhdat);
    
    KopirajDatoteko(imevhdat,imeizhdat);
    
	return 0;
}

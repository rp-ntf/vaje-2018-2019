#include <stdio.h>

/* Naloge za danes : "rp-ntf.gitlab.io" 

Sestavite program, 
ki prebere dve stevili, 
in vrne absolutno vrednost njune razlike

Sestavite program, 
katerega podatki vsebujejo koordinate ogljišč pravokotnika : 
imamo pravokotnik s koordinatami ogljišč (x1,y1) levo spodaj 
ter (x2,y2) desno zgoraj in točko T (x,y), x, y preberemo od uporabnika. 
Izpišite, ali leži točka znotraj pravokotnika 

Navodila : 
Sestavi program, ki za dve premici 
y = k1 * x + n1
y = k2 * x + n2
izpiše ali 
- se sekata v eni točki (in izpiše točko)
- se premici prekrivata 
- sta premici vzporedni 

Naloga : 
program prebere 3 koeficiente polinoma 2. stopnje 
p(x) = a * x^2 + b * x + c
in ugotovi, koliko realnih nicel ima. 
Vse nicle tudi izpise (tudi kompleksne nicle).
Kvadratni koren dobite iz knjiznice math.h : 
#include <math.h>
y = sqrt(x); // y je kvadratni koren stevila x
	
Domace naloge : 
1. Sestavi program, ki bo prebral sredisci in polmera dveh krogov, 
ter preveril, ali sta kroga locena, se dotikata ali pa se sekata. 

2. Sestavi program, ki bo prebral 3 realna stevila, 
in preveril, ali obstaja trikotnik s taksnimi dolzinami stranic. 
V primeru da obstaja, naj program izracuna njegovo ploscino in obseg.
Namig : heronov obrazec

*/
int main(int argc, char **argv)
{
	printf("hello world\n");
	return 0;
}

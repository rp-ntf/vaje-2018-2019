#include <stdio.h>


/* Naloga: Sestavi program, ki prebere štiri števila 
 * ter vrne največjega
 * */

int main()
{
	float x1,x2,x3,x4;
    
    printf("Vpisi stiri stevila: \n");
    scanf("%f %f %f %f",&x1,&x2,&x3,&x4);
    printf("Vpisali ste naslednja stevila:\n");
    printf(" x1=%g \n x2=%g \n x3=%g \n x4=%g\n",x1,x2,x3,x4);
    
    if ((x1>x2)&&(x1>x3)&&(x1>x4)) printf("najvecje stevilo je x1 (%g).\n",x1);
    else {
        if((x2>x1)&&(x2>x3)&&(x2>x4)) printf("najvecje stevilo je x2 (%g).\n",x2);
        else {
            if((x3>x1)&&(x3>x2)&&(x3>x4)) printf("najvecje stevilo je x3 (%g).\n",x3);
            else printf("najvecje stevilo je x4 (%g).\n",x4);
        }
    }
	return 0;
}

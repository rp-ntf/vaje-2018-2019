/* Sestavi program, ki naključno izbere število
 * med 1 in 100 ter zahteva od uporabnika, 
 * da to število ugane.
 * Program na vsakem koraku pove, ali je uporabnikovo
 * stevilo vecje ali manjse od ugibanega stevila
 * */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
    float stevilo;
    int racunalnik; // stevilo, ki ga izbere racunalnik
    int uporabnik; // uporabnikovo stevilo
    int i=0;
    srand((unsigned)time(NULL));
    
    stevilo = (float)rand()/RAND_MAX;
    racunalnik = 1 + (int)(100*stevilo);

    // Tu napisite svoj program
    while(1) 
    {
        // preberemo od uporabnika in mu povemo vecje manjse
        printf("Ugibaj ! ");
        scanf("%d",&uporabnik);
        if(uporabnik == racunalnik)
            break;
        if( uporabnik < racunalnik )
            printf("Vase stevilo je premajhno\n");
        else
            printf("Vase stevilo je preveliko\n");
    }
    printf("Bravo ! zadeli ste\n");
    return 0;
}












































    
/*
    printf("Izmislil sem si stevilo med 1 in 100. Ugani ga!\n");
    do {
        i++;
        printf("%d. poskus: ",i); scanf("%d",&uporabnik);
        if (racunalnik > uporabnik)
            printf("Narobe! Moje stevilo je vecje. \n");
        if (racunalnik < uporabnik)
            printf("Narobe! Moje stevilo je manjse. \n");
    } while (racunalnik != uporabnik);
    printf("Bravo! Uganil si v %d poskusih.\n",i);
*/

/* Naloge (napisi funkcijo in jo uporabi v main funkciji) : 
 * Sestavite funkcijo, ki sesteje 3 cela stevila
 * in vrne njihovo vsoto.
 * Sestavite funkcijo, ki vrne razliko 2 celih stevil.
 * Sestavite funkcijo, ki vrne kvocient dveh realnih stevil. 
 * Sestavite funkcijo, ki na zaslon izpise 
 * 3 znake, ki jih dobi kot argument.
*/
/*
 * Sestavite funkcijo, ki vrne produkt dveh vsot (a+b)*(c+d)
 * Sestavite funkcijo, ki izpise, ali je stevilo deljivo z 10
 * Sestavite funkcijo, ki izpise skupno dolzino 2 
 * nizov, ki jih prejme kot argument. Uporabite strlen
 * 
 * Sestavite funkcijo, ki vrne produkt 3 stevil
 * ce dobi a, b, c in d kot argumente
 * Sestavite funkcijo, ki na zaslon izpise, ali je stevilo sodo.
 * Sestavite funkcijo, ki izpise tabelo 5 stevil, 
 * ki jo dobi kot argument
 * Sestavite funkcijo, ki vrne vsoto elementov tabele 5 stevil
 * Sestavite funkcijo, ki vrne povprecje
	( realno stevilo) elementov tabele 5 stevil
*/

#include<stdio.h>
// <kaj vrne> <ime funkcije>( <tip1> <ime1>, <tip2> <ime2>)
// {
//    telo funkije
//    return <to kar funkcija vrne>; // ce funkcija ni void
// }

int sestejTriCelaStevila( int a, int b, int c )
{
    return a+b+c;
}

//Sestavite funkcijo, ki vrne razliko realnega in celega stevila.
float razlikaRealnegaInCelegaStevila( float x, int n )
{
    return x - n;
}

//Sestavi funkcijo, ki izpise tabelo 5 realnih stevil
void izpisiTabelo5( float tabela[5] )
{
    for( int i=0; i<5; ++i )
        printf("tabela[%d] = %f\n", i, tabela[i] );
}

//Sestavite funkcijo, ki podvoji vse elemente
//tabele 5 realnih stevil in vrne vsoto prvotne tabele
float podvojiTabelo( float tabela[5] )
{
    float vsota = 0;
    for( int i=0; i<5; ++i )
    {
        vsota+= tabela[i];
        tabela[i] = tabela[i] * 2;
    }
    return vsota;
}
//Sestavite funkcijo, ki pristeje 42 realnemu stevilu 
// in vrne vsoto
float pristej42( float x )
{
    x = x + 42;
    return x;
}

int main()
{
    int x=1, y=2, z=33;
    int vsota;
    vsota = sestejTriCelaStevila(x,y,z);
    printf("Vsota treh stevil je %d\n", vsota );
    float razlika = razlikaRealnegaInCelegaStevila(2.5,7);
    printf("Razlika je %f\n", razlika );
    float tabelaRealnihStevil[] = { 1., 2., 4., 5., 6. };
    izpisiTabelo5(tabelaRealnihStevil);
    float vsotaTabele = podvojiTabelo(tabelaRealnihStevil);
    printf("Vsota tabele = %f\n", vsotaTabele );
    izpisiTabelo5(tabelaRealnihStevil);
    
    float xx = 7, yy = 0.;
    printf("Pred funkcijo : xx = %f, yy = %f\n",
        xx, yy );
    yy = pristej42(xx);
    printf("Po funkciji   : xx = %f, yy = %f\n",
        xx, yy );
    
    return 0;
}


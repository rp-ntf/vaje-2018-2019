//Kazalci
#include <stdio.h>

void Zamenjaj(int *a, int *b) 
{
    int c;
    c =*a;
    *a=*b;
    *b=c;
}

int main()
{
	
   int x = 1;
   int y = 4;   
    
    Zamenjaj(&x,&y);
    
    printf("x = %d, y = %d\n",x,y);
    
	return 0;
}

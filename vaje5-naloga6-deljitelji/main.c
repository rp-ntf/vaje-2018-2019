#include <stdio.h>

/* Naloga:
 * sestavite program, ki izpiše vsa števila, ki delijo število (n).
 * Število n preberite od uporabnika.
 */


int main()
{
	int n, i, j=0;
    
    printf("Vpisi stevilo n: "); scanf("%d",&n);
    for (i = 2;i<n;i++) {
        if (n%i == 0) {                 // lahko tudi if (!(n%i))
            j++;
            printf("%d. delitelj stevila %d je %d\n",j,n,i);
        }
    }
    if (j==0) 
        printf("Stevilo %d je prastevilo.\n",n);
    else 
        printf("Stevilo %d ni prastevilo.\n",n);
        
        
	return 0;
}

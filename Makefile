.PHONY: clean All

All:
	@echo "----------Building project:[ vaje11-naloga1 - Debug ]----------"
	@cd "vaje11-naloga1" && "$(MAKE)" -f  "vaje11-naloga1.mk"
clean:
	@echo "----------Cleaning project:[ vaje11-naloga1 - Debug ]----------"
	@cd "vaje11-naloga1" && "$(MAKE)" -f  "vaje11-naloga1.mk" clean

/* Naloga : 
* Sestavite program, 
* ki od uporabnika najprej prebere dolzino tabele (najvec 100), 
* potem pa od uporabnika prebere toliko celih stevil.
* Ko so stevila prebrana, program najprej izpise tabelo v eni vrstici. 
* Potem pa program najde en primer dveh 
* stevil na zaporednih mestih v tabeli, 
* ki nista urejeni po velikosti,
* in ju uredi po velikosti. 
* Na koncu program izpise novo tabelo.
* Primer : 
* tabela dolzine 5 : 
* 1 2 4 3 2
* 1 2 3 4 2
* 
* Bonus naloga : 
* naj program zamenjuje dve zaporedni stevili vse dokler 
* tabela ni povsem urejena.
*/
/* Namig : dve zaporedni stevili v tabeli zamenjamo tako : 
* int zacasno;
* zacasno = tabela[i];
* tabela[i] = tabela[i+1];
* tabela[i+1] = zacasno;
*/       

#include <stdio.h>

int main()
{
    int n;
    int tabela[100];
    // preberemo dolzino tabele
    printf("Vnesite stevilo elementov tabele : ");
    scanf("%d", &n );
    
    // napolnimo tabelo
    for( int i=0; i<n; ++i )
    {
        printf("tabela[%d] = ", i );
        scanf("%d", &tabela[i] );
    }
    
    // Izpisemo tabelo v eni vrstici
    for( int i=0; i<n; ++i )
        printf("%d ", tabela[i]);
    printf("\n");
    
    int zacasno;
    for( int i=0; i<n-1; ++i )
    {
        if(tabela[i] > tabela[i+1]) // tabela je urejena pri <= 
        {
//            printf("Na mestu %d tabela ni urejena.\n",i);
//            printf("%d > %d\n", tabela[i], tabela[i+1]);
//            break;
            zacasno = tabela[i];
            tabela[i] = tabela[i+1];
            tabela[i+1] = zacasno;
        }
    }
    
    // Izpisemo tabelo v eni vrstici
    for( int i=0; i<n; ++i )
        printf("%d ", tabela[i]);
    printf("\n");
	return 0;
}













































/*
	printf("Vnesite dolzino tabele : ");
	int dolzina; 
	scanf("%d", &dolzina );
	int tabela[100]; // Tabela maksimalne dolzine 100
	int i;
	for( i=0; i<dolzina; ++i )
	{
		printf("Vnesite tabela[%d] = ", i );
		scanf("%d", &tabela[i] );
	}
	for( i=0; i<dolzina; ++i )
	{
		printf("%d ", tabela[i] );
	}
	printf("\n");
	// Najdemo prvi primer, kjer je : tabela[i] > tabela[i+1]
	int zacasno;
	for( i=0; i<dolzina-1; ++i )
	{
		if( tabela[i] > tabela[i+1] )
		{
			// Zamenjamo vrednos
			zacasno = tabela[i];
			tabela[i] = tabela[i+1];
			tabela[i+1] = zacasno;
			// prekinemo zanko
			break;
		}
	}
	printf("Deloma urejena tabela : \n");
	for( i=0; i<dolzina; ++i )
	{
		printf("%d ", tabela[i] );
	}
	printf("\n");
	return 0;
}

*/

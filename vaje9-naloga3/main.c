// Tabele in kazalci
#include <stdio.h>

/*  int tabela[] = {32, 33, 1, 3,4};   naslov: &tabela[0],   &tabela[i],  vrednost: tabela[0], tabela[i]
 *   int *tabela =  {32, 33, 1, 3,4};    naslov: tabela, tabela + i,  vrednost: *tabela, *(tabela+i)
*/

void funkcija_A(int a, int *k1, int *k2)
{
    *k1 = a*a;
    *k2 = a*a*a;
}

int main()
{
 
    int x, y;
    int a = 5;
    
    funkcija_A(a,&x,&y);
    
    printf("x = %d, y = %d\n",x,y);
    
	return 0;
}

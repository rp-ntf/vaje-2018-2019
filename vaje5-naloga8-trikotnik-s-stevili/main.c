#include <stdio.h>
/*
Sestavi program, ki na zaslon izpiše takšno piramido:
         1
        232
       34543
      4567654
     567898765
    67890109876
   7890123210987
  890123454321098
 90123456765432109
0123456789876543210
*/

int main()
{
    int n;
    printf("Vpisite visino trikotnika : ");
    scanf("%d", &n );
    int cifra;
    for( int i=0; i<n; ++i ) // gre po vrsticah
    {
        cifra = i+1;
        for(int j=0; j<n-1-i; ++j )
            printf(" ");
        /*
        for(int j=0; j<i+1; ++j )
        {
            printf("%d",cifra%10);
            cifra = cifra + 1;
        }
        cifra = cifra - 2;
        for(int j=0; j<i; ++j )
        {
            printf("%d",cifra%10);
            cifra =  cifra -1;
        }
         * */
        for(int j=0; j<2*i+1; ++j )
        {
            printf("%d",cifra%10);
            if( j<i)
                cifra = cifra + 1;
            else 
                cifra = cifra - 1;
        }
        printf("\n");
    }
	return 0;
}

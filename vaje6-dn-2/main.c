
/* 2. Naloga:
* Sestavi program, ki prebere koordinate dveh vektorjev
* v prostoru in vrne njuno vsoto in njun skalarni produkt.
*/

#include <stdio.h>

int main()
{
	float v1[3], v2[3], v_vsota[3];
	float skalarni_produkt;
	for( int i=0; i<3; ++i )
	{
		printf("Vnesite v1[%d] : ",i);
		scanf("%f",&v1[i]);
	}
	for( int i=0; i<3; ++i )
	{
		printf("Vnesite v2[%d] : ",i);
		scanf("%f",&v2[i]);
	}

	// Izracunamo vsoto:
	for( int i=0; i<3; ++i )
		v_vsota[i] = v1[i] + v2[i];
	// Izpisemo vsoto
	printf("v1 + v2 = ( %f, %f, %f )\n",
			v_vsota[0], v_vsota[1], v_vsota[2] );

	// Izracunamo skalarni produkt:
	skalarni_produkt = 0.;
	for( int i=0; i<3; ++i )
		skalarni_produkt += v1[i] * v2[i];
	printf("v1 . v2 = %f\n", skalarni_produkt );
			
	printf("hello world\n");
	return 0;
}

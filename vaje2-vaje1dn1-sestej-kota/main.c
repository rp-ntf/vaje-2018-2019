#include <stdio.h>

/*
 * Sestavite program, 
 * ki od uproabnika prebere dva kota v stopinjah, minutah in sekundah. 
 * Kota seštejte in izpišite rezultat v stopinjah, minutah in sekundah.

Namig : lahko ponovno uporabite kodo za pretvorbo med jardi in metri.
 */

int main()
{
    int stop1, stop2, min1, min2, sek1, sek2;
    int stop3, min3, sek3;
    
	printf("Vnesite prvi kot v stopinjah minutah in sekundah : ");
	scanf("%d %d %d", &stop1, &min1, &sek1 );
	printf("Vnesite drugi kot v stopinjah minutah in sekundah : ");
	scanf("%d %d %d", &stop2, &min2, &sek2 );
    
    // Skupna vsota v sekundah
    sek3 = sek1 + sek2 + 60*(min1+min2) + 60*60*(stop1+stop2);
    
    // izracunamo skupne minute in stopinje
    min3 = sek3 / 60;
    sek3 = sek3 - min3*60;
    
    stop3 = min3 / 60;
    min3 = min3 - stop3 * 60;
    
    printf("Vsota kotov je \n%d %d %d\n", stop3, min3, sek3 );
	
	return 0;
}

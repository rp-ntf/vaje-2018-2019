#include <stdio.h>
/* Napiši program, ki izpiše vse popolne kube
 * manjše od števila n
 * */
 
int main()
{
	int n, x=1, kub=1;
    
    printf("Vpisi zgornjo mejo: "); scanf("%d",&n);
    
    while (kub<n) {
        printf("%d\n",kub);
        x++;
        kub=x*x*x;
    }
    
	return 0;
}

/* Napiši program, ki bo izpisal prazen pravokotnik
 * sestavljen iz zvezdic (*) velikosti a x b (stevili preberite od uporabnika)
 * Primer:
a=4, b=6
******
*    *
*    *
*    *
******
 Napiši program, ki bo izpisal prazen enakokrak trikotnik
 * sestavljen iz zvezdic (*) visine n (stevilo preberite od uporabnika)
 primer n=5
    *
   * *
  *   *
 *     *
*********
Sestavi program, ki naključno izbere število
 * med 1 in 100 ter zahteva od uporabnika, 
 * da to število ugane.
Sestavite program, ki izpiše vsa števila, ki delijo število (n).
 * Število n preberite od uporabnika.
Sestavi program, ki bo za dano naravno število
 * preveril, ali je to število praštevilo
Sestavi program, ki na zaslon izpiše takšno piramido:
         1                        rp-ntf.gitlab.io
        232
       34543
      4567654
     567898765
    67890109876
   7890123210987
  890123454321098
 90123456765432109
0123456789876543210
stevilo vrstic piramide preberite od uporabnika
Sestavi program: Fibonaccijevo zaporedje je je rekurzivno določeno z naslednjim pred-
pisom F_n = F_(n−1) +F_(n−2) , kjer je F_n n-to Fibonaccijevo število. Sestavi
program, ki izpiše prvih n Fibonaccijevih števil, če je F_1 = F_2 = 1.
Sestavite program,
 * ki s pomočjo razvoja v vrsto izračuna vrednost
 * funkcije
 * exp(x) = sum_k=0..n (x^k / k!)
 *
 * Seštejte samo prvih 100 členov
 */
// Domaci nalogi:
/* Sestavi program, ki na zaslon izpiše
 * piramido velikosti n, kjer uporabnik vnese
 * število n. Piramida:
         *
       ***
     *****
   *******
Sestavi program: Na zaslon izpiši zvezdice in sicer tako,
 * da bodo tvorile krog s polmerom
 * n zvezdic.
 * */

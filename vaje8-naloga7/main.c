#include <stdio.h>

int main()
{
	/* Naloga : 
	 * sestavite program, ki izpisuje igralno polje krizcev 
     * in krozcev.
	 * Program naj ima 2D tabelo znakov, 
     * na vsakem koraku pa naj uporabnika vprasa 
	 * za vrstico in stolpec, kamor naj postavi naslednji znak. 
	 * V tabelo izmenicno postavlja znake 'X' in 'O', 
	 * in na vsakem koraku tabelo tudi izpise. 
	 * Programu ni potrebno preveriti, ali se je igra ze koncala.
     * (Bonus : preverite
	 * Program se konca, ko uporabnik vnese vrstico 
     * ali stolpec izven obmocja 0-2.
	 */
	char igralnoPolje[3][3]; // 2D tabela znakov
	int i, j; // Zacnemo s praznim poljem :
	for( i=0; i<3; ++i )
		for( j=0; j<3; ++j )
			igralnoPolje[i][j] = '_'; // Presledek
	int vrstica=0, stolpec=0;
	char XaliO = 'X';
	while( 1 )
	{
		// Izpisi igralno polje : 
		printf("Igralno polje je : \n");
		for( i=0; i<3; ++i )
		{
			for( j=0; j<3; ++j )
				printf("%c ", igralnoPolje[i][j] );
			printf("\n");
		}
        
		// preberi od uporabnika vrstico in stolpec, in nastavi ustrezni element tabele na 'X' ali 'O'
		printf("Vnesite vrstico : ");
		scanf("%d", &vrstica );
		if( vrstica > 2 ||vrstica < 0 )
			break;
		printf("Vnesite stolpec : ");
		scanf("%d", &stolpec );
		if( stolpec > 2 ||stolpec < 0 )
			break;
		igralnoPolje[vrstica][stolpec] = XaliO;
        
		if( XaliO == 'X' )
			XaliO = 'O';
		else
			XaliO = 'X';
		// Bonus : ali je v vrstici "vrstica" 3 X ?
		/* int stevilo_x, stevilo_o;
		for( i=0; i<3; ++i )
			if( igralnoPolje[i][0] == 'X' )
				stevilo_x++; */
	}
	/*
	 * Bonus naloga : 
	 * Sestavi enak program za 4-v-vrsto, ki ima igralno polje 5x7, 
	 * in vsak zeton pade do konca na tla.
	 * 
	 * Bonus naloga : 
	 * program naj pri obeh igrah preverja, ali se je igra ze koncala.
	 */
	return 0;
}

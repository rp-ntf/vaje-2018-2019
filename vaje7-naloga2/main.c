#include <stdio.h>

int main()
{
    /* Naloga :
     * sestavite program, ki od uporabnika prebere 5 stevil.
     * Ta stevila naj shrani v 1. stolpec 2D tabele.
     * 
     * V 2. in 3. stolpec 2D tabele naj program shrani 2 in 3 kratnike 
     * stevil v 1. stolpcu tabele.
     * 
     * Program naj na koncu izpise celotno 2D tabelo,
     * tako da so v isti vrstici stevilo in njegovi mnogokratniki.
     * 
     */
    
    // 2D tabela
    int tabela[3][5];
    /* 
       int tabela[3][5] = { 
        { 1, 2, 3, 4, 5 }, 
        { 2, 4, 6, 8, 10 },
        { 3, 6, 9, 12, 15 } };
    */
    // NI OK : tabela[4][0]
    // tabela[0,1,2][0,1,2,3,4]
    // tabela[i][j] // spremenljivka tipa int
    for( int i=0; i<5; ++i )
    {
        printf("Vnesite stevilo %d : ", i );
        scanf("%d", &tabela[0][i] );
    }
    /*
    for( int i=0; i<5; ++i )
    {
        tabela[1][i] = 2 * tabela[0][i];
        tabela[2][i] = 3 * tabela[0][i];
    }
     */
    for( int i=0; i<5; ++i )
    {
        for( int j=1; j<3; ++j )
            tabela[j][i] = (j+1) * tabela[0][i];
    }
    
    for( int i=0; i<5; ++i )
    {
        printf("%d | ", i );
        for( int j=0; j<3; ++j )
            printf("%d ", tabela[j][i] );
        printf("\n");
    }
    
    
     
    return 0;
}


void aaa()
{
    // int tabela1[5]; // tabela1[0] ... tabela1[4]
    int tabela[3][5]; 
    // <tip spremenljivke v tabeli> ime_spremenljivke[<velikost>][<velikost>];
    // tabela[0] je tabela velikosti 5
    // tabela[1][2] je spremenljivka tipa int
    
    // tabela[1][3]; // se obnasa kot spremenljivka tipa int
    printf("Vnesite 5 celih stevil v tabelo : \n");
    for( int i=0; i<5; ++i )
    {
        printf("Vnesite stevilo %d : ", i );
        // scanf("%d", &x ); // preberemo vnos v spremenljivko x
        scanf("%d", &tabela[0][i] ); // preberemo vnos v spremenljivko x
    }
    for( int i=0; i<5; ++i )
    {
        tabela[1][i] = 2*tabela[0][i];
        tabela[2][i] = 3*tabela[0][i];
    }
    for( int i=0; i<5; ++i )
    {
        // printf("tabela1[%d] = %d\n", i, tabela1[i] );
        printf("%d : %d | %d | %d \n", i, 
            tabela[0][i], tabela[1][i], tabela[2][i] );
    }
	return 0;
}


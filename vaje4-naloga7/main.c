#include <stdio.h>
/* Sestavi program, kjer uporabnik vpisuje pozitivna števila,
 * dokler ne vpiše števila 0. Ko vpiše uporabnik število 0
 * se izvajanje zanke konča, program pa vpiše povprečno 
 * vrednost vpisanih števil.
 * */
 
int main()
{
    int vsota=0;
    int x, stevec=0;
  
    do {
        printf("Vpisi pozitivno stevilo, ali 0, ce zelis koncati: "); scanf("%d",&x);
        vsota = vsota + x;
        if (x != 0) stevec++;
    } while (x != 0);
    
    if (stevec != 0) printf("Povprecje vpisanih stevil je %g.\n",(float)vsota/stevec);
     
	return 0;
}

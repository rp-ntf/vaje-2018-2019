#include <stdio.h>
/* Naloga : 
* Izpisite stevilske vrednosti vseh znakov v angleski abecedi.
* ( Namig : for zanka : for( znak='A'; znak <= 'Z' ; ++znak )
* Izpisite velike in male crke.
*/
/* Naloga : 
* Izpisite vse mozne znake. Namig : zanka med 0 in 255
*/
/* Naloga : 
* Preverite, kaj se zgodi, ce zelite izpisati 
* znakovno vrednost stevil nad 255. 
* Primerjajte te znakovne vrednosti z vrednostmi obicajnih znakov.
*/

int main()
{

    char znak;
	for( znak='A'; znak <= 'Z'; ++znak )
		printf("Znak %c ima stevilsko vrednost %d\n", znak, znak );
	for( znak='a'; znak <= 'z'; ++znak )
		printf("Znak %c ima stevilsko vrednost %d\n", znak, znak );
	
    int iznak;
	for( iznak=0; iznak<255; ++iznak )
		printf("Znak %c ima stevilsko vrednost %d\n", iznak, iznak );
    for( iznak=255; iznak<512; ++iznak )
		printf("Znak %c ima stevilsko vrednost %d\n", iznak, iznak );
	return 0;
}


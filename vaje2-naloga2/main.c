/* Nova snov : 
 * logični stavki
 * pogojni stavki
 */

#include <stdio.h>

int main()
{
	// Pogojni stavki v jeziku C :
	printf("if\n");
	// Stavek if : 
	// if (pogoj) stavek ;
	// "stavek" se izvrši le, če je pogoj resničen
    // { stavek1; stavek2; ..., stavekN; }
	// pogoj je lahko LOGIČEN IZRAZ
	
	// Logični izrazi (več o tem spodaj) :
	// x == y je res, natanko takrat, ko sta x in y enaka
	
	if(1==1)
		printf("1 == 1 je resnicno\n");
	if(1==0)
		printf("1 == 0 je resnicno\n");
	
	// Stavek if .. else :
	printf("if .. else\n");
	/* if(pogojo)
	 * 	stavek_RES;
	 * else
	 * 	staven_NI_RES;
	 *
	 * stavek_RES se izvrši, če je "pogoj" resničen, 
	 * če ni, se izvrši stavek_NI_RES
	 */
	if(1 == 0)
		printf("1 == 0 je resnicno\n");
	else
		printf("1 == 0 ni resnicno\n");
	
	printf("if..else if.. else if..else\n");
	/* Stavek if .. else if .. else if .. else
	 * if(pogoj1)
	 * 	stavek1;
	 * else if(pogoj2)
	 * 	stavek2;
	 * else if(pogoj3)
	 * 	stavek3;
	 * else
	 * 	stavekSicer;
	 * 
	 * Če je res pogojX, se izvrši stavekX, 
	 * če nobeden od pogojeX ni res, se izvrši stavekSicer.
	 * Izvrši se natanko edeno od stavek1,stavek2,...,stavekX,stavekSicer.
	 */
	if(1 == 0)
		printf("1 == 0\n");
	else if(1 == 1 )
		printf("1 == 1\n");
	else if(1 == 2 )
		printf("1 == 2\n");
    else if(2 == 2)
        printf("2 == 2 je resnicno\n"); // ta se nikoli ne izpise
	else
		printf("Nic od nastetega ni res\n");
	
	/* Logicni izrazi :
	 * Logicen izraz je navaden izraz s stevilsko vrednostjo,
	 * pravimo da je "res", ce je njegova vrednost razlicna od 0.
	 
	 x < y : res, ce je x strogo manjsi od y
	 x > y : res, ce je x strogo vecji od y
	 x <= y : res, ce je x manjsi ali enak od y
	 x >= y : res, ce je x vecji ali enak od y
	 x == y : res, ce sta x in y natanko enaka
	 x != y : res, ce sta x in y razlicna
	 
	 * Logicne izraze lahko kombiniramo z uporabo logicnih operatorjev v daljse izraze
	 ( izraz1 ) && ( izraz2 ) : logični in, 
	    skupen izraz je resničen natanko tedaj, ko sta oba izraza resnicna
	 ( izraz1 ) || ( izraz2 ) : logicni ali,
	    skupen izraz je resnicen natanko tedaj, ko je vsaj eden od obeh izrazov resnicen
	 !(izraz) : logicna negacija,
	    skupen izraz je resnicen natanko tedaj, ko je izraz neresnicen

	 */ 
	 // Primeri :
	printf("Izraz : (1==0) && (1==1) je ");
	if( (1==0) && (1==1) )
		printf("resnicen\n");
	else 
		printf("neresnicen\n");
		
	printf("Izraz : (1==0) || (1==1) je ");
	if( (1==0) || (1==1) )
		printf("resnicen\n");
	else 
		printf("neresnicen\n");

	printf("Izraz : !(1==0) je ");
	if( !(1==0) )
		printf("resnicen\n");
	else 
		printf("neresnicen\n");
	
	// Pogojni izrazi
	/* pogoj ? izraz_RES : izraz_NI_RES ;
	 * 
	 * vrednost pogojnega izraza je izraz_RES v primeru, da je pogoj resnicen
	 * in izraz_NI_RES ce pogoj ni resnicen.
	 * Primeri :
	 */
	printf("Manjsi izmed 10, 6 je %d\n", (10<6) ? 10 : 6 );
	printf("Vecji  izmed 10, 6 je %d\n", (10>6) ? 10 : 6 );
    
    // Pogojne stavke lahko tudi gnezdimo
    if( 5 > 1 && 5 <= 10 )
    {
        if( 5 < 5 )
            printf("5 < 5\n");
        else 
            printf("5 < 5 ni res\n");
    }
	
	return 0;
}

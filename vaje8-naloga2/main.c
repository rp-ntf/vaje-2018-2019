#include <stdio.h>
#include <string.h>
int main()
{
	/* Naloga : 
	 * Naredite program, ki od uporabnika v zanki 
	 * prebira niz, in izpise ta niz skupaj z njegovo dolzino. 
	 * Program se konca, ko uporabnik vpise niz "konec".
	 */
     
     char niz[256];
     char niz_konec[256] = "konec";
     do {
         printf("Vpisite niz : \n");
         scanf("%s", niz );
         printf("Niz %s ima %d znakov.\n", niz, strlen(niz) );
     } while( strcmp(niz,niz_konec) != 0 );
     printf("Vpisali ste niz %s, koncujem.\n", niz );

    return 0;
}

	/* Naloga : 
	 * Naredite program, ki od uporabnika pebere 2 niza in ju primerja
	 * po slovarskem redu.
	 * Na zaslon naj izpiše niz, ki v slovarju pride prej
	 * namig: uporabite strcmp
	 */
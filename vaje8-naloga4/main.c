#include <stdio.h>

/* Funkcije
 * Do zdaj smo spoznali ze nekaj funkcij, na primer : 
 * main(...)
 * printf(...)
 * scanf(...)
 * srlen(..)
 * Te funkcije so definirane v kodi ki jo 
 * v program vkljucimo z ustreznim #include <nekaj.h>
 * Funkcije pa lahko pisemo tudi sami.
 * Definiramo jih zunaj glavne funkcije programa (PRED main !) : 
 */
// int moja_prva_funkcija(int pristej)
// vrne     ime_funkcije ( tip_podatka ime_podatka)
int mojaPrvaFunkcija(int pristej);
// vrnemo vrednost "return" stavka.
int mojaPrvaFunkcija(int pristej)
{
	int rezultat = pristej + 42;
	printf("42 + %d = %d\n", pristej, rezultat );
	return rezultat;
}
// mojaPrvaFunkcija(1); // OK, 1 je int
// mojaPrvaFunkcija('A'); // OK funkcija dobi stevilsko vrednost 'A'
// mojaPrvaFunkcija(1.2345); // OK funkcija dobi celo stevilsko vrednost
// mojaPrvaFunkcija(1,2,3); // NE GRE
/* Funkcija moja_prva_funkcija tako pristeje vrednost, 
 * ki jo dobi v oklepajih stevilu 42, izpise rezultat in racun, 
 * ter vrne rezultat.
 * Funkcije lahko sprejmejo vec podatkov, 
 * razlicnih podatkovnih tipov.
 * Npr :
 */
int vsota( int x, float y )
{
	return x + y;
}
/* Posebnega pomena je beseda "void", ki pomeni "nic" 
 * v smislu da funkcija ne vrne nicesar, 
 * ali pa nicesar ne sprejme kot argument :
 */
void nicNeVrnem( int a, int zzz, char ttt )
{
	printf("Dobim argumente %d, %d, %c in ne vrnem nicesar.\n", a, zzz, ttt );
}
int vrni_42(void)
{
	return 42;
}
int spet_vrni_42() // tudi tako gre. Besedo void pri argumentih lahko izpustimo.
{
	return 42;
}

// Funkcije lahko kot argument prejmejo tudi tabele :
void izpisi_tabelo_3( int tab[3] )
{
	int i;
	for( i=0; i<3; ++i )
		printf("%d ", tab[i]);
	printf("\n");
}

int main(int argc, char **argv)
{
	int i;
	printf( "Moja prva funkcija vrne : %d\n", 
        mojaPrvaFunkcija(7)
    );
	printf( "Vsota 1 in 2 je %d\n", 
        vsota( 1, 2 ) );
	printf( "Klicem funkcijo ki nic ne vrne\n");
	nicNeVrnem( 1, 2, 'F' );
	printf("Vrnem 42 : %d\n", vrni_42() );
	printf("Spet vrnem 42 : %d\n", spet_vrni_42() );
	printf("Klicem izpis tabele : \n");
	int tab[3]= { 10 , 11, 12 };
	izpisi_tabelo_3(tab);
	return 0;
}

#include <stdio.h>

/* Naloga:
 * Sestavi program, ki prebere v tabelo 10 celih števil
 * in poišče največji in najmanjši element tabele ter ju izpiše
 * */

int main()
{
    int a[10];
    int n=10;
    int i;
    int min, max;
    
    for (i=0;i<n;i++) {
        printf("Vpisi %d. celo stevilo: ",i+1); scanf("%d",&a[i]);
    }
    
    min=max=a[0];
    
    for (i=1; i<n;i++) {
        if (a[i]>max) 
            max=a[i];
        if (a[i]<min)
            min =a[i];
    }
    
    printf("max = %d  in min = %d\n",max,min);
    
	return 0;
}

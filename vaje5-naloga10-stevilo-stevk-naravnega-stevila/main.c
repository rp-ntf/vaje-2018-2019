
/* Naloga:
 * Sestavi program, ki prebere naravno število n
 * in izpiše število njegovih števk.
 * Namig: uporabi zanko do
 * */

#include <stdio.h>

int main()
{
    int i=0, n;
    
    printf("Vpisi naravno stevilo n: "); scanf("%d",&n);
    
    do {
       n = n/10;
       i++;
    } while (n!=0);
    
    printf("Vpisano stevilo ima %d stevk.\n",i);

	return 0;
}

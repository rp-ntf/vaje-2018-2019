#include <stdio.h>

/*
Domace naloge : 
1. Sestavi program, ki bo prebral sredisci in polmera dveh krogov, 
ter preveril, ali sta kroga locena, se dotikata ali pa se sekata. 
 */

#include <math.h> // Za fabs funkcijo, sqrt

int main()
{
	float x1, y1, r1, x2, y2, r2,dx,dy,d;
    printf("Vnesite x, y, r kroga 1 : ");
    scanf("%f %f %f", &x1, &y1, &r1 );
    printf("Vnesli ste krog (%f,%f), R = %f\n",
        x1, y1, r1 );
    printf("Vnesite x, y, r kroga 2 : ");
    scanf("%f %f %f", &x2, &y2, &r2 );
    printf("Vnesli ste krog (%f,%f), R = %f\n",
        x2, y2, r2 );
    
    dx = x1 - x2;
    dy = y1 - y2;
    d = sqrt( dx*dx + dy*dy );
    if( d > r1 + r2 )
        printf("Kroga sta locena (povsem narazen)\n");
    else if( d < fabs(r1-r2) )
        printf("Kroga sta locena (eden znotraj drugega)\n");
    else if( d == (r1+r2) )
        printf("Kroga se dotikata\n");
    else
        printf("Kroga se sekata\n");

	return 0;
}



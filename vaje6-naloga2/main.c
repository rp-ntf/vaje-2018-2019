#include <stdio.h>

/* Naloga:
 * Sestavi program, ki prebere 5 števil in jih izpiše
 * v obratnem vrstnem redu
 * */

int main()
{
	int Nejc[5];
    int n=5;
    int i;
    
    for (i=0;i<n;i++) {
        printf("Vpisi %d. celo stevilo: ",i+1); scanf("%d",&Nejc[i]);
    }
    
    printf("\n");
    
    for(i=n-1;i>=0;i--) {
        printf("element %d:  %d\n",i+1,Nejc[i]);
    }
	return 0;
}

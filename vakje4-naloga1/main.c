#include <stdio.h>

/* Napiši program, ki bo izpisal pravokotnik
 * sestavljen iz zvezdic (*) velikosti 10 X 5.
 * */
 
int main()
{
	int i=0, j=0;
    
    while (j<5) {
            i = 0;
            while (i<10) {
                printf("*");
                i++;                           // i=i+1;
            }
            printf("\n");
            j=j+1;                          // j++;
    }

    
    printf("\n");
	return 0;
}

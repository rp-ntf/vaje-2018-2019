#include <stdio.h>

/*
 * Naloga:
 * izračunaj kot pod katerim leti nabit 
 * delec po izhodu iz kondenzatorja.
 * Kondenzator je "idealen" kondenzator, in za to deluje
 * na delec tako, da ves čas ko je delec v kondenzatorju
 * na delec deluje konstantna sila F0 (parameter kondenzatorja).
 * 
 * Na začetku delec leti vodoravno s hitrostjo v0.
 * Kondenzator je dolg 1m in je postavljen
 * med 2. in 3m stran od koordinatnega izhodišča.
 * 
 * Gravitacija na delec ne deluje.
 * Zračni upor na delec ne deluje.
 * 
 * Izracuni naj bodo narejeni v funkciji, in trajektorijo delca
 * naj o shranjena v datoteko.
 * Funkcija sprejme argumente:
 * - maso delca,
 * - silo kondenzatorja
 * - zacetno hitrost delca
 * - ime datoteke za izpis
 */

void izracunajTrajektorijoDelca(
    float m,
    float F0,
    float v0,
    char * imeDatoteke
)
{
    FILE * f = fopen(imeDatoteke,"wb");
    if( f == NULL )
    {
        printf("Napaka: datoteke %s ne moremo odpreti\n", imeDatoteke);
        return;
    }
    float x, y, vx, vy;
    float dt = 0.0001;
    x = 0;
    y = 0;
    vx = v0;
    vy = 0;
    do {
        x += vx * dt;
        y += vy * dt;
        vx = vx;
        if( x > 1.0 && x < 2.0 ) // smo v kondenzatorju
            vy += F0/m;
        else if( x > 3.0 && y < 3.5 ) // smo v drugem kondenzatorju
            vy += (-3*F0)/m;
        fprintf( f, "%f, %f, %f, %f\n", x, y, vx, vy );
    } while( x < 4.0 );
    fclose(f);
}

/* Naloga:
 * med 3. in 3.5. metrom od koord. izhodišča je še en kondenzator,
 * trikrat večje moči, obrnjen v ravno obratno stran.
 * Izračunaj trajektorijo delca enako kot prej in rezultat izpiši v datoteko
 */

int main()
{
	izracunajTrajektorijoDelca(
        0.01,
        0.0001,
        10,
        "../trajektorija.dat"
    );
	return 0;
}

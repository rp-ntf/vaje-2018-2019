#include <stdio.h>

/*
2. Sestavi program, ki bo prebral 3 realna stevila, 
in preveril, ali obstaja trikotnik s taksnimi dolzinami stranic. 
V primeru da obstaja, naj program izracuna njegovo ploscino in obseg.
Namig : heronov obrazec
 * */
#include <math.h> // Za fabs funkcijo

int main()
{
	float a, b, c, s, P;
    printf("Vnesite tri realna stevila a, b, c : ");
    scanf("%f %f %f", &a, &b, &c );
    printf("Vnesli ste stevila %f, %f, %f\n",
        a, b, c );
    
    if( (a < b+c) && (b < a+c) && (c<a+b) &&
        (a > fabs(b-c)) && (b > fabs(a-c)) && ( c > fabs(a-b) ) )
    {
        printf("Stevila a = %f, b = %f, c = %f lahko tvorijo stranice trikotnika\n", a, b, c );
        s = (a+b+c)/2.0;
        P = sqrt( s * (s-a) * (s-b) * (s-c) );
        printf("Tak trikotnik ima ploscino %f\n", P );
    }
    else
        printf("Ne obstaja trikotnik, ki bi imel stranice danih velikosti\n");
    
	return 0;
}
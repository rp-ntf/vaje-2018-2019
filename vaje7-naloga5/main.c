
/* Naloga : 
* Sestavite program, 
* ki od uporabnika najprej prebere dolzino tabele (najvec 100), 
* potem pa od uporabnika prebere toliko celih stevil.
* 
* Naj zaporedje stevil uredi in urejeno tabelo izpise na zaslon.
*
* NAMIG: uporabi kodo prejsne naloge, kjer urejanje ponovis veckrat
*/

#include <stdio.h>

int main()
{
    int n;
    int tabela[100];
    // preberemo dolzino tabele
    printf("Vnesite stevilo elementov tabele : ");
    scanf("%d", &n );
    
    // napolnimo tabelo
    for( int i=0; i<n; ++i )
    {
        printf("tabela[%d] = ", i );
        scanf("%d", &tabela[i] );
    }
    
    // Izpisemo tabelo v eni vrstici
    for( int i=0; i<n; ++i )
        printf("%d ", tabela[i]);
    printf("\n");
    
    int zacasno;
    for( int ponovitev = 0; ponovitev < n*n; ponovitev++ )
    {
        for( int i=0; i<n-1; ++i )
        {
            if(tabela[i] > tabela[i+1]) // tabela je urejena pri <= 
            {
                zacasno = tabela[i];
                tabela[i] = tabela[i+1];
                tabela[i+1] = zacasno;
            }
        }
    }
    
    // Izpisemo tabelo v eni vrstici
    for( int i=0; i<n; ++i )
        printf("%d ", tabela[i]);
    printf("\n");
	return 0;
}



































































/*
	printf("Vnesite dolzino tabele : ");
	int dolzina; 
	scanf("%d", &dolzina );
	int tabela[100]; // Tabela maksimalne dolzine 100
	int i;
	for( i=0; i<dolzina; ++i )
	{
		printf("Vnesite tabela[%d] = ", i );
		scanf("%d", &tabela[i] );
	}
	for( i=0; i<dolzina; ++i )
	{
		printf("%d ", tabela[i] );
	}
	printf("\n");
	// Najdemo prvi primer, kjer je : tabela[i] > tabela[i+1]
	int zacasno;
    for( int j=0; j<dolzina*dolzina; ++j )
    {
        for( i=0; i<dolzina-1; ++i )
        {
            if( tabela[i] > tabela[i+1] )
            {
                // Zamenjamo vrednos
                zacasno = tabela[i];
                tabela[i] = tabela[i+1];
                tabela[i+1] = zacasno;
                // prekinemo zanko
                break;
            }
        }
    }
	printf("Deloma urejena tabela : \n");
	for( i=0; i<dolzina; ++i )
	{
		printf("%d ", tabela[i] );
	}
	printf("\n");
	return 0;
}
*/

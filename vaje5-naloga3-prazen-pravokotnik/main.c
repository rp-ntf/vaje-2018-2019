#include <stdio.h>

/* Napiši program, ki bo izpisal prazen pravokotnik
 * sestavljen iz zvezdic (*) 
 * velikosti a x b (stevili preberite od uporabnika)
 * Primer:
 
a=4, b=6
******
*    *
*    *
*    *
******
 * */

int main()
{
    int a,b;
    printf("Vnesite stevilo vrstic pravokotnika : ");
    scanf("%d",&a);
    printf("Vnesite stevilo stoplcev pravokotnika : ");
    scanf("%d",&b);
    printf("Izrisali bomo pravokotnik dimenzije %d x %d\n", a, b);
    
    // prva vrstica:
    for(int j=0; j<b; ++j )
        printf("*");
    printf("\n");
    for(int i=1; i<a-1; ++i )
    {
        printf("*");
        for(int j=1; j<b-1; ++j )
        {
            printf(" ");
        }
        printf("*");
        printf("\n");
    }
    // zadnja vrstica:
    for(int j=0; j<b; ++j )
        printf("*");
    printf("\n");
    
    printf("\n\n\n");

	return 0;
}

/* Naloga:
 * napisite program, ki uporabnika vprasa za dolzino njegovega imena.
 * Potem ime prebere v tabelo znakov (max 100), znak po znak.
 * Ime potem izpise v obratnem vrstnem redu crk.
 */
#include <stdio.h>

int main()
{
    char znak;
    int dolzina;
    char tabela[100];
    
    printf("Kako dolgo ime imas ? ");
    scanf("%d", &dolzina );
    printf("Berem ime dolzine %d, znak po znak\n", dolzina );

    for( int i=0; i<dolzina && i<100; i++)
    {
        // printf("Berem znak %i\n", i );
        scanf(" %c", &znak ); // ! presledek pred %c, da lahko preberemo samo en znak in ne preberemo znaka za prehod v novo vrstico
        tabela[i] = znak;
        // printf("zank %i je %c / %c\n", i, tabela[i], znak);
    }
    
    printf("\n");
    for( int i=dolzina-1; i>=0; --i )
        printf("%c", tabela[i] );
    printf("\n");
    
    
    
	
	return 0;
}

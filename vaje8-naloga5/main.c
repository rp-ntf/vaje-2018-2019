#include <stdio.h>
#include <string.h>

/* Naloge (napisi funkcijo in jo uporabi v main funkciji) : 
 * Sestavite funkcijo, ki sesteje 3 cela stevila
 * in vrne njihovo vsoto.
 * Sestavite funkcijo, ki vrne razliko 2 celih stevil.
 * Sestavite funkcijo, ki vrne kvocient dveh realnih stevil. 
 * Sestavite funkcijo, ki na zaslon izpise 
 * 3 znake, ki jih dobi kot argument.
*/

int sestejTri( int stevilo1, int stevilo2, int stevilo3 )
{
    return stevilo1 + stevilo2 + stevilo3;
}
int razlikaDvehStevil( int stevilo1, int stevilo2 )
{
    return stevilo1 - stevilo2;
}
float kvocient( float stevilo1, float stevilo2 )
{
    return stevilo1 / stevilo2;
}
void izpisiTriZnake( char c1, char c2, char c3 )
{
    printf("%c%c%c", c1, c2, c3 );
}

/*
 * Sestavite funkcijo, ki vrne produkt dveh vsot (a+b)*(c+d)
 * Sestavite funkcijo, ki izpise, ali je stevilo deljivo z 10
 * Sestavite funkcijo, ki izpise skupno dolzino 2 
 * nizov, ki jih prejme kot argument. Uporabite strlen
 * 
 * Sestavite funkcijo, ki vrne produkt 3 stevil
 * ce dobi a, b, c kot argumente
 
*/

// <kaj vrne> <ime funkcije>( <tip1> <ime1>, <tip2> <ime2>)
// {
//    telo funkije
//    return <to kar funkcija vrne>; // ce funkcija ni void
// }



int produktDvehVsot( int a, int b, int c, int d )
{
    return (a+b) * (c+d);
}
void aliJeSteviloDeljivoZDeset( int stevilo )
{
    if(stevilo % 10 == 0 )
        printf("Stevilo %d je deljivo z 10.\n", stevilo );
    else
        printf("Stevilo %d ni deljivo z 10.\n", stevilo );
}
void aliJeDeljivoZDeset(int stevilo)
{
	if( stevilo % 10 == 0 )
		printf("Stevilo je deljivo z 10\n");
	else 
		printf("Stevilo ni deljivo z 10\n");
}
int skupnaDolzinaDvehNizov( char niz1[], char niz2[] )
{
    return strlen(niz1) + strlen(niz2);
}
int produktTrehStevil( int a, int b, int c )
{
    return a*b*c;
}
//* Sestavite funkcijo, ki na zaslon izpise, ali je stevilo sodo.
void aliJeDeljivoZDva(int stevilo)
{
    if(stevilo%2 == 0 )
        printf("Stevilo %d je sodo\n",stevilo);
    else
        printf("Stevilo %d je liho\n", stevilo );
}
// * Sestavite funkcijo, ki izpise tabelo 5 stevil, 
// * ki jo dobi kot argument
void izpisiTabelo5Stevil( int tabela[5] )
{
    for( int i=0; i<5; ++i )
        printf("tabela[%d] = %d\n", i, tabela[i] );
}
// * Sestavite funkcijo, ki vrne vsoto elementov tabele 5 stevil
int sestejTabelo( int tabela[5] )
{
    int vsota = 0;
    for( int i=0; i<5; ++i )
        vsota += tabela[i];
    return vsota;
}
// * Sestavite funkcijo, ki vrne povprecje
//	( realno stevilo) elementov tabele 5 stevil
// Sestavi funkcijo, ki izpise vsote elementov dveh stevil pri istih indeksih
// Sestavi funkcijo, ki vrne produkt 3 stevil iz tabele 
// Sestavi funkcijo, ki vrne najvecji element tabele poljubne dolzine
// Sestavi funkcijo, ki vrne povprecje tabele realnih stevil poljubne dolzine


    

int main(int argc, char **argv)
{
    aliJeSteviloDeljivoZDeset( 100 );
    printf("Produkt dveh vsot = %d\n", produktDvehVsot( 1, 2, 3, 4 ) );
    printf( "Skupna dolzina dveh nizov je %d\n", skupnaDolzinaDvehNizov( "abcdefghij", "ABCDEF" ) );
    printf("Produkt treh stevil je %d\n", produktTrehStevil(1, 2, 3 ) );
    aliJeDeljivoZDva(3);
    aliJeDeljivoZDva(223);
    int tabela[5] = { 2, 4, 6, 8, 111 };
    izpisiTabelo5Stevil(tabela);
    printf("Vsota stevil v tabeli je %d\n", sestejTabelo(tabela) );
    return 0;
    
    
    
    
    
	int vsota;
	vsota = sestejTri(1,2,3);
	printf("Vsota 1, 2 in 3 je : %d\n", vsota );
	izpisiTriZnake('a','e','i');
	printf("TUKAJ\n");
	int n;
	n = produktDvehVsot(1,2,3,4);
	printf("Produkt dveh vsot je : %d\n", n );
	printf("Produkt dveh vsot je : %d\n", produktDvehVsot(1,2,3,4) );
	printf("Preverjamo deljivost z 10 stevila 27:\n");
	aliJeDeljivoZDeset(27);
	printf("Skupna dolzina nizov:\n");
	char niz1[256] = "abcdef";
	char niz2[256] = "ghijklmno";
	skupnaDolzinaDvehNizov(niz1, niz2);
	skupnaDolzinaDvehNizov("test", "spettest");
	return 0;
}


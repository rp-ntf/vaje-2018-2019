#include <stdio.h>

/*
Sestavite program, ki od uporabnika prebere 3 mestno število. 
Število obrnite in izpišite na zaslon. 
Primer : Uporabnik vpiše število 591. 
Vaš program naj izpiše število 195.

Namig : lahko ponovno uporabite kodo za pretvarjanje med jardi in metri, 
in na koncu obrnete vrstni red izpisa metrov, decimetrov in centimetrov.
*/

int main()
{
	int n;
	int stotice, desetice, enice;
	printf("Vpisite tromestno stevilo : ");
	scanf("%d", &n);
	stotice = n / 100;
	n = n - stotice * 100;
	desetice = n / 10;
	n = n - desetice * 10;
	enice = n;

	printf("Obrnjeno stevilo je %d%d%d.\n", enice, desetice, stotice );
	
    // int novo_stevilo = 100*enice + 10*desetice + stotice
    // printf("%d", novo_stevilo);
    
	return 0;
}

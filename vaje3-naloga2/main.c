#include <stdio.h>

/* Sestavi program, ki prebere rezultate treh kolokvijev. 
 * Računalnik naj pove, ali je uporabnik naredil pisni del izpita, 
 * če so kriteriji za pozitivno oceno naslednji:
* pisni izpit je pozitiven, če so vsi trije 
* kolokviji pozitivni, ali če sta dva pozitivna in je povprečje 
* vseh treh kolokvijev večje ali enako 50%. Vsak posamezen 
* kolokvij se šteje za pozitivnega, če je študent dosegel najmanj 40%.
* */

int main()
{
	float k1,k2,k3;
    printf("Vpisi rezultate kolokvijev v %.\n");
    printf("1.kolokvij: "); scanf("%f",&k1);
    printf("2.kolokvij: "); scanf("%f",&k2);
    printf("3.kolokvij: "); scanf("%f",&k3);
    
    if ((k1>=40.0)&&(k2>=40.0)&&(k3>=40.0)) 
        printf("Pisni izpit ste opravili s povprečjem %g\%\n",(k1+k2+k3)/3);
    else if (((k1+k2+k3)>=150.0)&&(((k1>=40)&&(k2>=40))||((k1>=40)&&(k3>=40))||((k2>=40)&&(k3>=40)))) 
        printf("Pisni izpit ste opravili s povprečjem %g\%\n",(k1+k2+k3)/3);
    else 
        printf("Pisnega izpita niste opravili. Vase povprecje je %g\%\n",(k1+k2+k3)/3);
    
	return 0;
}

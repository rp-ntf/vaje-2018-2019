#include <stdio.h>

// funkcija
// <tip ki ga vrne (void)> <ime funkcije (izpisi_tabelo)>( <tip argumenta 1> <ime argumenta 1>, ... )
// { ...; ...; return <vrednost>; }
void izpisi_tabelo( int tabela[], int velikost_tabele )
{
    printf("Izpisujemo tabelo velikost %d\n", velikost_tabele );
    for( int i=0; i<velikost_tabele; ++i )
        printf("%d | %d\n", i, tabela[i] );
}

// Naloga:
// 1. Napisi funkcijo, ki na zaslon izpise n zvezdic v eni vrstici, n je argument funkcije
// 2. Napisi funkcijo, ki na zaslon izpise pravokotnik m x n (n vrstic) z uporabo prejsne funkcije iz 1. naloge
void izpisi_vrstico_zvezdic( int n )
{
    for( int i=0; i<n; ++i )
        printf("*");
    printf("\n");
}
void izpisi_pravokotnik( int stVrstic, int stStolpcev )
{
    for( int i=0; i<stVrstic; ++i )
        izpisi_vrstico_zvezdic(stStolpcev);
}

// 3. Napisi funkcijo, ki na zaslon izpise najprej x presledkov, potem y zvezdic. Npr (x=3, y=2):
// ___** (_ je kot presledek)
void izpisi_presledke_in_zvezdice( int x, int y )
{
    for( int i=0; i<x; ++i )
        printf(" ");
    for( int i=0; i<y; ++i )
        printf("*");
    printf("\n");
}
// 4. Napisi funkcijo, ki na zaslon izpise enakokrak trikotnik poln zvezdic visine n (z uporabo prejsne funkcije)
void izpisi_poln_trikotnik( int n )
{
    for( int i=0; i<n; ++i )
        izpisi_presledke_in_zvezdice( n-i-1, 2*i+1);
}
// 5. Napisi funkcijo, ki na zaslon izpise najprej x presledkov, eno zvezdico, y presledkov in se eno zvezdico.
//     Ce je y negativen, se druge zvezdice ne izpise
// Npr: x = 3, y=-2:
// ___*
// Npr x=5, y=3:
// _____*___*
// 6. Z uproabo funkcije 5 napisite funkcijo, ki na zaslon izpise votel enakokrak trikotnik visine n
// z uporabo funkcij 5. in 3.
void izpisi_presledke_zvezdico_presledke_zvezdico( int x, int y )
{
    for( int i=0; i<x; ++i )
        printf(" ");
    printf("*");
    if( y<0 )
    {
        printf("\n");
        return;
    }
    for( int i=0; i<y; ++i )
        printf(" ");
    printf("*");
    printf("\n");
}
void izpisi_votel_trikotnik( int n )
{
    for( int i=0; i<n-1; ++i )
        izpisi_presledke_zvezdico_presledke_zvezdico( n-i-1, 2*i+1-2 );
    int i=n-1;
        izpisi_presledke_in_zvezdice( n-i-1, 2*i+1);
}
// 7. Napišite funkcijo, ki vrne vsoto kvadratov prvih n naravnih stevil
// 7a) z zanko
int vsota_kvadratov_z_zanko( int n )
{
    int vsota = 0;
    for( int i=1; i<=n; ++i )
        vsota += i*i;
    return vsota;
}
// 7b) z rekurzijo
int vsota_kvadratov_z_rekurzijo( int n )
{
    if( n== 1 )
        return 1;
    else
        return n*n + vsota_kvadratov_z_rekurzijo( n-1 );
}
// 8. Napisite funkcijo, ki vrne n-to fibonacijevo stevilo (z rekurizjo) (f_n+1 = f_n + f_n-1)
int fib( int n )
{
    if( n==1 )
        return 1;
    if( n==2 )
        return 1;
    return fib(n-1) + fib(n-2);
}
// 9. Napisite funkcijo, ki vrne skalarni produkt dveh seznamov realnih stevil tipa float
float skalarni_produkt( float x[], float y[], int n )
{
    float skalarniProdukt = 0;
    for( int i=0; i<n; ++i )
        skalarniProdukt += x[i] * y[i];
    return skalarniProdukt;
}
// 10. Napisite funkcijo, ki izracuna produkt matrike z matriko in rezultat shrani v matriko, ki jo dobi kot zadnji argument
//     ( vse matrike so velikosti 5x5 )
void produkt_matrik( float a[5][5], float b[5][5], float c[5][5] )
{
    for( int i=0; i<5; ++i )
        for( int j=0; j<5; ++j )
        {
            c[i][j] = 0.0;
            for( int k=0; k<5; ++k )
                c[i][j] = a[i][k] * b[k][j];
        }
}
// 11. Napisite funkcijo, ki po velikosti uredi seznam dolzine 2 (dve celi stevili v eni tabeli)
void uredi_2( int seznam[2] )
{
    if( seznam[0] < seznam[1] )
        return; // ze urejeno
    // ce nista urejena ju zamenjam
    int tmp = seznam[0];
    seznam[0] = seznam[1];
    seznam[1] = tmp;
}
// 12. Z uporabo funkcije 11 napisite funkcijo, ki popolnoma uredi seznam celih stevil poljubne dolzine
void uredi_seznam( int seznam[], int n )
{
    for( int i=0; i<n; ++i )
        for( int j=0; j<n-1; ++j )
            uredi_2( &seznam[j] );
}
// Domaca naloga:
// 13. Napisite funkcijo, ki z rekurizjo najde vse prastevilske deljitelje danega stevila, in izpise faktorizacijo stevila
// 14. Napisite funkcijo, ki najde najmanjsi skupni veckratnik in najvecji skupni deljitelj dveh stevil, ki jih dobi kot argument

int main()
{
    // Tabele:
    int tabela1[10];
    int tabela[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    int novaTabela[5] = { 2, 4, 6, 8, 10 };
        
    // izpisi_tabelo( tabela, 10 );
    // izpisi_tabelo( novaTabela, 5 );
    // izpisi_vrstico_zvezdic(4);
    // izpisi_pravokotnik(6,8);
    izpisi_poln_trikotnik( 5 );
    izpisi_votel_trikotnik( 5 );
    printf("Vsota kvadratov stevil do 10 = %d\n", vsota_kvadratov_z_rekurzijo(10) );
    printf("Vsota kvadratov stevil do 10 = %d\n", vsota_kvadratov_z_zanko(10) );
    printf("fib(10) = %d\n", fib(10) );
    int seznam[6] = { 22, 11, 53, 101, 2, -4 };
    izpisi_tabelo(seznam,6);
    uredi_seznam(seznam,6);
    izpisi_tabelo(seznam,6);
	return 0;
}

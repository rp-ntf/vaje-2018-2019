#include <stdio.h>
#include <string.h>
int main()
{
    /* Naloga : 
	 * Naredite program, ki od uporabnika pebere 2 niza in ju primerja
	 * po slovarskem redu.
	 * Na zaslon naj izpiše niz, ki v slovarju pride prej
	 * namig: uporabite strcmp
	 */
     char niz1[256];
     char niz2[256];
     
     printf("Vpisite niz 1 : \n");
     scanf("%s", niz1 );
     printf("Vpisite niz 2 : \n");
     scanf("%s", niz2 );
     
     int primerjava;
     primerjava = strcmp(niz1,niz2);
     if(primerjava == 0)
         printf("Niza sta enaka. Noben niz ");
    else if(primerjava < 0)
        printf("%s", niz1);
    else
        printf("%s", niz2 );
     printf(" je v slovarju prej.\n");
     
    return 0;
}

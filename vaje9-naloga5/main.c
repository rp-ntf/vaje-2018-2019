/* Sestavi funkcijo, ki kot parameter prejme velikost tabele celih števil,
 * rezervira prostor za tabelo in vrne kazalec na to tabelo. Funkcijo uporabi v programu, 
 * najprej v tabelo vpiše kvadrate števil od 1 do velikosti tabele in jih izpiše v 
 * obratnem vrstnem redu. Na koncu sprostite spomin.
 * */
#include <stdio.h>
#include <stdlib.h>

int *tabelaINT(int n)
{
    int *kaz = (int *)malloc(n*sizeof(int));
    
    return kaz;
}

int main()
{
    int x = 5;
    int *tabela = tabelaINT(x);
    
    for (int i=0; i<x;i++)
        tabela[i]=(i+1)*(i+1);
	
    for (int i=4; i>=0;i--)
        printf("%d\n",tabela[i]);
        
    free(tabela);
	return 0;
}

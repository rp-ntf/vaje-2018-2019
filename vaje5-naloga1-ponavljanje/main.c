#include <stdio.h>

// Ponavljanje zank (in nova snov)

int main(int argc, char **argv)
{
    int i;
    
    printf("Ponavljanje zank:\n");
    
    printf("Zanka for:\n");
    for( i=0; i<10; ++i ) // (pozor ! tu ni ";")
    {
        printf("For : i = %d\n", i);
    }
    printf("\n");
    
    printf("Zanka while:\n");
    i=0; 
    while(i<10) // (pozor ! tu ni ";")
    {
        printf("While : i = %d\n", i );
        i++;
    }
    printf("\n");
    
    printf("Zanka do-while:\n");
    i=0;
    do {
        printf("Do-While : i = %d\n", i);
        i++;
    } while(i<10); // (pozor ! tu je ";")

    
    printf("\n\n\n");
    printf("Nova snov: break in continue\n");

    printf("Zanka for:\n");
    for( i=0; i<10; ++i ) // (pozor ! tu ni ";")
    {
        if(i<4)
        {
            printf("i = %d, i<4, continue\n", i);
            continue;
        }
        if(i==7)
        {
            printf("i==7, break\n");
            break;
        }
        printf("For : i = %d\n", i);
    }
    printf("\n");
    
    printf("Zanka while:\n");
    i=0; 
    while(i<10) // (pozor ! tu ni ";")
    {
        if(i<4)
        {
            printf("i = %d, i<4, continue AFTER i++\n", i);
            i++;
            continue;
        }
        if(i==7)
        {
            printf("i==7, break\n");
            break;
        }
        printf("While : i = %d\n", i );
        i++;
    }
    printf("\n");
    
    printf("Zanka do-while:\n");
    i=0;
    do {
        if(i<4)
        {
            printf("i = %d, i<4, continue AFTER i++\n", i);
            i++;
            continue;
        }
        if(i==7)
        {
            printf("i==7, break\n");
            break;
        }
        printf("Do-While : i = %d\n", i);
        i++;
    } while(i<10); // (pozor ! tu je ";")

	return 0;
}

#include <stdio.h>

/* Sestavi program, ki izpiše in prešteje
 * vse delitelje števila n.
 * */
 
int main()
{
    int n;
    int stevec = 0;
    int delitelj=1;
	printf("Vpisi naravno stevilo n: "); scanf("%d",&n);
    
    while(delitelj <= n) {
        if (n % delitelj == 0) {
            printf("%d\n",delitelj);
            stevec = stevec + 1;              // ++stevec, stevec++
        }
        delitelj++;                                     // delitelj = delitelj + 1;
    }
    
    printf("\nDeliteljev stevila %d je %d.\n",n,stevec);
    if (stevec==2) 
        printf("Stevilo %d je prastevilo.\n",n);
    else
        printf("Stevilo %d ni prastevilo.\n",n);
	return 0;
}

#include <stdio.h>

/*
Fibonaccijevo zaporedje je je rekurzivno določeno z naslednjim pred-
pisom F_n = F_(n−1) +F_(n−2) , kjer je F_n n-to Fibonaccijevo število. Sestavi
program, ki izpiše prvih n Fibonaccijevih števil, če je F_1 = F_2 = 1.
*/

int main()
{
    int n;
    int f1=1, f2=1, f3;
	printf("Vpisi stevilo n: "); scanf("%d",&n);
    
    printf("1\n1\n");
    
    for (int i=2; i<n;i++) {
        f3 = f1 + f2;
        printf("%d\n",f3);
        f1 = f2;
        f2 = f3;
    }
    
	return 0;
}

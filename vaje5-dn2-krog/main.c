/* Na zaslon izpiši zvezdice in sicer tako,
 * da bodo tvorile krog s polmerom
 * n zvezdic.
 * */

int main()
{
	int r = 10;

    for (int i = 0; i < 2*r; i++) {
        for (int j = 0; j < 2*r; j++) {
            if ( (j-r)*(j-r) + (i-r)*(i-r) <= r*r)
                printf("*");
            else
                printf(" ");
        }
        printf("\n");
    }

	return 0;
}


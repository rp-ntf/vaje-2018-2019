#include <stdio.h>

/* Domaca naloga : 
* Naredite program, 
* ki od uporabnika prebere niz znakov, 
* a) in izpise, kolikokrat se v njem pojavi crka 'a'.
* for( int i=0; i<strlen(niz); ++i )
* { if( niz[i] == 'a' ) stevec++ }
* b) in izpise, kolikokrat se v njem pojavi samoglasnik. 
* Primer : 
* To je testen stavek 
* stevilo samoglasnikov : 6
*/

int main(int argc, char **argv)
{
	printf("hello world\n");
	return 0;
}

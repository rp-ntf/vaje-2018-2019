#include <stdio.h>
/* Serstavi program, ki izpiše vse lihe kvadrate med
 * 1 in n.
 * */
 
int main()
{
	int i,n,kvadrat=1;
    
    printf("Vpisi zgornjo mejo: "); scanf("%d",&n);
    
    for(i=1; kvadrat<n;i++) {
        kvadrat=i*i;
        if (kvadrat%2 == 1) printf("%d\n",kvadrat);
    }
    
	return 0;
}

/* Domača naloga
Sestavi funkcijo, ki kot parameter prejeme:
a) ime datoteke z rezultatom
b) kot meta kamna
c) začetno višino in
d) začetno hitrost
ter v datoteko zapiše trajektorijo meta kamna (točke x,y). 
Gostoto točk določi sam (max 100).
Zračnega upora ne upoštevaj.
*/
#include <stdio.h>
#include <math.h>

void IzracunajVodoravniMet(
    char * imeDatoteke,
    float kotMetaVStopinjah,
    float zacetnaVisina,
    float zacetnaHitrost
)
{
    FILE * f = fopen(imeDatoteke,"wb");
    float x,y, vx, vy;
    float dt = 0.01;
    int stevec = 0;

    // 1. možnost: izračunamo x(t) in y(t) "na roko"
    // in iz te enačbe izpisujemo koordinate ob določenih časih
    // 2. možnost: naprogramiramo integrator (manj točno)
    x = 0;
    y = zacetnaVisina;
    vx = cos( kotMetaVStopinjah / 180. * M_PI ) * zacetnaHitrost;
    vy = sin( kotMetaVStopinjah / 180. * M_PI ) * zacetnaHitrost;
    
    do {
        x += dt * vx;
        y += dt * vy;
        
        vx = vx;
        vy = vy -9.81 * dt;
        
        // Zracni upor?
        /*
        v = sqrt(vx*vx+vy*vy)
        vx -= k*v*v;
        vy -= k*v*v;
         * */
        
        stevec++;
        
        fprintf( f, "%d, %f, %f , %f, %f \n", stevec, x, y, vx, vy );
    } while( y > 0 );
    

    fclose(f);
}

int main(void)
{
    char imeDatoteke[] = "../datotekaVodoravniMet.dat";
    IzracunajVodoravniMet(
        imeDatoteke,
        60.,
        10.,
        30. );
    return 0;
}



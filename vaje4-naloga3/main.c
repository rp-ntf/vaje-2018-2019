#include <stdio.h>

/* Sestavi program, ki bo na zaslon
 * izpisal naslednji lik velikosti n, ki ga določi
 * uporabnik:
                             * 
                             * *
                             * **
                             * ***
                             ...
 * */

int main()
{
	int i=0, j, n;
    
    printf("Vpisi visino lika: "); scanf("%d",&n);
    
    while (i<n) {
        j=0;
        while (j <=i) {
            printf("*");
            j++;
        }
        printf("\n");
        i++;
    }
    
    printf("\n");
	return 0;
}

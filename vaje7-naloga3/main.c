/* Naloga:
 * sestavite program, ki od uporabnika najprej prebere 
 * stevilo dimenzij prostora 
 * (<50),
 * potem pa se dva vektroja v tem prostoru.
 * Program naj izpise skalarni produkt teh dveh vektorjev na zaslon.
 * a . b = a0 * b0 + a1*b1 + a2*b2 + a3*b3 + a4*b4
 * 
 * Program naj izpise skalarni produkt prvega vektorja z 
 * drugim vektorjem vzetim v obratnem vrstnem redu komponent
 * a # b = a0 * b4 + a1*b3 + a2*b2 + a3*b1 + a4*b0
 */
#include <stdio.h>

int main()
{
    int n;
    int a[50], b[50];
    int skalarni_produkt;
    int obrnjen_skalarni_produkt;
    printf("Povej stevilo dimenzij (<50) : ");
    scanf("%d", &n );
    if( n>=50 )
        n = 50;
    printf("Izbrali ste %d dimenzij.\n", n);
    for( int i=0; i<n; ++i )
    {
        printf("Vnesite prvi vektor [%d] : ", i );
        scanf("%d", &a[i]); // a[0], a[1], ... a[n-1]
    }
    for( int i=0; i<n; ++i )
    {
        printf("Vnesite drugi vektor [%d] : ", i );
        scanf("%d", &b[i] );
    }
    
    skalarni_produkt = 0;
    for( int i=0; i<n; ++i )
        skalarni_produkt += a[i] * b[i];
    printf("a . b = %d\n", skalarni_produkt); 
    
    obrnjen_skalarni_produkt = 0;
    for( int i=0; i<n; ++i )
        obrnjen_skalarni_produkt += a[i] * b[n-i-1];
    printf("a # b = %d\n", obrnjen_skalarni_produkt );

	return 0;
}

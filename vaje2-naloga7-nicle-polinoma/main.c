#include <stdio.h>
#include <math.h>

int main(int argc, char **argv)
{
     /*int p = 3, q = 2;
     float pq1 = p/q;
     float pq2 = ((float)p)/q;
     printf(" 3 / 2 brez (float) = %f\n", pq1 );
     printf(" 3 / 2 z (float)    = %f\n", pq2 );*/
     
	/* Naloga : 
	 * program prebere 3 koeficiente polinoma 2. stopnje 
	 * p(x) = a * x^2 + b * x + c
	 * in ugotovi, koliko realnih nicel ima. 
	 * Vse nicle tudi izpise (tudi kompleksne nicle).
	 * Kvadratni koren dobite iz knjiznice math.h : 
	 * #include <math.h>
	 * y = sqrt(x); // y je kvadratni koren stevila x
	 */
	 printf("Vpisite koeficiente polinoma druge stopnje : \n");
	 float a, b, c;
	 scanf("%f %f %f", &a, &b, &c );
	 printf("Prebran polinom : \n");
	 printf("p(x) = %f * x^2 + %f * x + %f\n", a, b, c );
	 
	 float D, x1, x2, sqrtD;
	 float x1re, x1im, x2re, x2im;
	 D = b*b - 4 * a * c;
	 if( D < 0.0 )
	 {
		 printf("Polinom nima realnih nicel\n");
		 sqrtD = sqrt( -D );
		 x1re = -b/(2*a);
		 x2re = -b/(2*a);
		 x1im = (+sqrtD) /(2*a); 
		 x2im = (-sqrtD) /(2*a); 
		 printf("x1 = %f + %f * i, x2 = %f + %f * i\n", x1re, x1im, x2re, x2im );
	 }
	 else if( D == 0.0 )
	 {
		 printf("Polinom ima eno realno niclo\n");
		 sqrtD = sqrt(D);
		 x1 = (-b+sqrtD) /(2*a); 
		 printf("x1 = %f\n", x1 );
	 }
	 else
	 {
		 printf("Polinom ima 2 realni nicli\n");
		 sqrtD = sqrt(D);
		 x1 = (-b+sqrtD) /(2*a); 
		 x2 = (-b-sqrtD) /(2*a); 
		 printf("x1 = %f, x2 = %f\n", x1, x2 );
	 }
	
	return 0;
}

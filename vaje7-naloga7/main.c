#include <stdio.h>

int main()
{
	int i;
	/* Znaki : 
	 * v programskem jeziku C obstaja spremenljivka tipa "znak" : */
	char znak;
	// Ko ji prirejujemo vrednost na nek znak, 
    // moramo ta znak podati v 'X' enojnih oklepajih
	znak = 'A';
	// Ko izpisujemo / beremo znake, uporabimo formatno dolocilo "%c"
	// c - character = znak
	printf("Znak = %c\n", znak );
	// Znaki pa imajo tudi svojo stevilsko vrednost. 
	// Znaku lahko priredimo njegovo stevilsko vrednost 
	znak = 75;
	printf("Znak = %c, stevilska vrednost %d\n", znak, znak );
	// S formatnim dolocilom za cela stevila %d, 
    // lahko izpisemo stevilsko vrednost znaka.
	// Smiselne stevilske vrednosti za znak so med 0 in 127. 
	// Lahko pa znaku priredimo vrednosti do 255.
	
	/* Obstajajo posebni znaki. 
	 * Izpis v novo vrsto pomeni znak "newline" = '\n'
	 * Kadar zelimo dobiti znakovno vrednost nekega posebnega znaka, 
	 * moramo pred ta znak postaviti znak \, ki pomeni, da se mora naslednji znak smatrati kot poseben.
	 * Primeri posebnih znakov : 
	 * '\n' - nova vrstica 
	 * '\r' - vrni se na zacetek vrstice
	 * '\'' - enojni narekovaj
	 * '\"' - dvojni narekovaj
	 * '\\' - znak \
	 * '\t' - tabulator 
	 * '\0' - znak s stevilsko vrednostjo 0, '\0' = 0
	 */
	// Spremenljivka tipa znak se obnasa kot obicajne spremenljivke, 
	// lahko ji prirejamo vrednosti in jo spreminjamo
	for( znak='A'; znak < 'A' + 5; znak++ )
		printf("Znak %c = %d\n", znak, znak );

	return 0;
}

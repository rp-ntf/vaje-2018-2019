#include <stdio.h>
/* Napiši program, ki bo izpisal prazen enakokrak trikotnik
 * sestavljen iz zvezdic (*) visine n (stevilo preberite od uporabnika)
 primer n=5
    *
   * *
  *   *
 *     *
*********
1) pravokotni trikotnik:
*
**
***
****
*****
2) pravokotni trikotnik v drugo smer
    *
   **
  ***
 ****
*****
3) enakokraki trikotnik (=2+1)
    **
   ****
  ******
 ********
**********
4) popravljen enakokraki trikotnik
    *
   ***
  *****
 *******
*********
5) izvotlen A
    *
   * *
  *   *
 *     *
*       *
6) izvotlen A s spodnjo crto
    *
   * *
  *   *
 *     *
*********
*/
int main()
{
    int n;
    printf("Vpisite visino trikotnika : ");
    scanf("%d", &n );
	for(int i=0; i<n-1; ++i)
    {
        // Tu popravimo
        { // LEVA polovica trikotnika
            for(int j=0; j<n-1-i; ++j )
                printf(" ");
            printf("*");
            for(int j=0; j<i; ++j )
                printf(" ");
        }
        { // DESNA polovica trikotnika
            //for(int j=0; j<i+1; ++j )
            for(int j=0; j<i-1; ++j ) // eno manj da je na vrhu 1x*
                printf(" ");
            if( i>0 )
                printf("*");
        }
        printf("\n");
    }
    for( int i=0; i<2*n-1; ++i )
        printf("*");
    printf("\n");
	return 0;
}

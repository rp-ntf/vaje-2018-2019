#include <stdio.h>

/* Naloga :
 * sestavite program, ki od uporabnika prebere 5 celih stevil.
 * Ta stevila naj shrani v tabelo.
 * 
 * Program naj naredi se dve tabeli, 
 * v tabelo 2. naj vstavi dvakratnike stevil v tabeli 1,
 * v tabelo 3. pa naj vstavi 3 kratnike stevil v tabeli 1.
 * Program naj na koncu izpise vse tri tabele,
 * z ustrezujocimi indeksi v isti vrstici.
 */
int main()
{   
    int tabela1[5];
    // int tabela[5] = { 0, 1, 2, 3, 4 };
    for( int i=0; i<5; ++i )
    {
        printf("Vnesite tabela[%d] : ", i );
        scanf("%d", &tabela1[i]);
    }
    int tabela2[5], tabela3[5];
    for( int i=0; i<5; ++i )
        tabela2[i] = 2 * tabela1[i];
    for( int i=0; i<5; ++i )
        tabela3[i] = 3 * tabela1[i];
    
    for( int i=0; i<5; ++i )
        printf("%d | %d | %d | %d |\n",
            i, tabela1[i], tabela2[i], tabela3[i] );
    
    return 0;
}

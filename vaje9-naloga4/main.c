// Dinamična alokacija spomina
#include <stdio.h>
#include <stdlib.h>

// void *malloc(<število bajtov>)
// void *calloc(<število elementov>, <velikost elementa>)
// void *realloc(void *kaz, <število bajtov>)
// size_t sizeof(tip)
// void free(void *kaz)

int main()
{
	float *dinTabela;
    int velikostTabele = 10;
    
    dinTabela = (float *)malloc(velikostTabele*sizeof(float));
    
    for(int i = 0; i<velikostTabele;i++)
        dinTabela[i]=2.1*i+1;
        
    for(int i=0; i<velikostTabele;i++)
        printf("%g\n",dinTabela[i]);
    
    free(dinTabela);
    
	return 0;
}


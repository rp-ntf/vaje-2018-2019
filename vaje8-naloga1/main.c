#include <stdio.h>
#include <string.h>

int main()
{
	int i;
	// S tabelami znakov lahko delamo enako kot delamo 
    // s tabelami drugih spremenljivk 
	char tabela_znakov[5] = { 'A', 'B', 67, 68, 'E' };
	// Pri tem da lahko v tabeli mesamo med prirejanjem 
    // znakov s pomocjo 'A' in stevil.
	printf("Izpisujemo tabelo 5 znakov : \n");
	for( i=0; i<5; ++i )
	{
		printf("tabela_znakov[%d] = %c | %d\n", 
            i, tabela_znakov[i], tabela_znakov[i] );
	}
	// Lahko pa tabele znakov uporabimo kot NIZE ZNAKOV.
	// Pomembno je : NIZ ZNAKOV se vedno konca 
    // s stevilsko vrednostjo 0 !
	// Tabela znakov se lahko konca s cimerkoli (kot vse tabele). 
	// Da tabela postane pravilen niz, se mora koncati z 0.
	char niz_znakov[6] = { 'a', 'b', 'c', 'd', 'e', 0 }; 
            //!! Pomembna je 0 na koncu
	// Ce je tabela znakov tudi niz, jo lahko izpisemo na poseben nacin.
	// Nize znakov lahko izpisujemo / preberemo 
    // od uporabnika s pomocjo formatnega dolocila '%s'
	// s - string = niz
	printf("Nas niz je = %s\n", niz_znakov ); // !!! ni niz_znakov[1]
	// Kadar nize beremo s tipkovnice, moramo poskrbeti za to, 
    // da je tabela za niz dovolj velika. 
	// Na primer : 
	char tabela_za_niz[256]; //<- 256 je recimo dovolj
	printf("Vnesite svoje ime : ");
	scanf("%s", tabela_za_niz ); // !!! TU NI & ZNAKA !!!
	printf("Vase ime je %s\n", tabela_za_niz );
	
	// Nize lahko deklariramo na poseben nacin kot : 
	char niz1[] = "TEST1"; // tabela dolzine 6 !!
	char *niz2 = "TEST2";
	// V obeh primerih je ustvarjena tabela znakov, ki vsebuje : 
	// { 'T', 'E', 'S', 'T', '1', 0 }
	// Prevajalnik za nas doda zakljucek niza ( 0 ) v tabelo znakov.
	
	// Za delo z nizi lahko uporabimo posebne funkcije, 
	// ki jih dobimo v 
	// #include <string.h>
	
	// int strlen( char niz[] ) : pove dolzino niza
	// int strchr( char niz[], char znak ) : pove na katerem mestu niza se znak najprej pojavi
	// int strcmp( char niz1[], char niz2[] ) : pove kateri niz je v slovarju prej :
	// negativna vrednost : niz1, 0 : oba sta enaka, pozitivna vrednost : niz2
	
	// Primer : 
	printf("Vnesite prvo ime : ");
	char ime1[256];
	scanf("%s", ime1 );
	char ime2[256];
	printf("Vnesite drugo ime : ");
	scanf("%s", ime2 );
	printf("ime 1 je %s in je dolgo %d znakov\n", ime1, strlen(ime1) );
	int urejenost_slovar = strcmp( ime1, ime2 );
	if( urejenost_slovar < 0 )
		printf("ime %s pride v slovarju pred imenom %s\n", ime1, ime2 );
	else if( urejenost_slovar > 0 )
		printf( "ime %s pride v slovarju za imenom %s\n", ime1, ime2 );
	else 
		printf("Obe imeni sta enaki : %s = %s\n", ime1, ime2 );
	return 0;
}

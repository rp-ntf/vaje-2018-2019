#include <stdio.h>

// 1. Sestavi funkcijo ki prejme kot parameter število celih števil, ki jih naj 
// zapiše v datoteko. Ime datoteke uporabnik poda kot parameter

void ZapisiCelaStevila(int n, char *imeDat)
{
    FILE *datoteka = fopen(imeDat,"wt");
    
    if (datoteka == NULL) printf("Datoteke ne morem odpreti!\n");
    else {
        for(int i=1;i<=n;i++)
            fprintf(datoteka,"%d, %d\n",i, i*i*i);
            
        fclose(datoteka);
    }
}

// 2. Sestavi funkcijo, ki prejme kot parameter ime datoteke in
// prebere števila ter jih izpiše na zaslon. Če datoteke ne more odpreti
// naj o tem obvesti uporabnika
 
void BeriCelaStevila(char *imeDat)
{
    FILE *branje = fopen(imeDat, "rt");
    int x, i=0;
    
    if (branje == NULL) printf("Napaka! Ne morem odpreti datoteke.\n");
    else {
        while(1) {
            fscanf(branje,"%d",&x);
            if(feof(branje)) break;
            printf("%d\n",x);
            i++;
        }
        printf("Prebrano stevilo vnosov: %d\n",i);
        fclose(branje);
    }
}

int main()
{
    char imedatoteke[10];
    int n;
    
    printf("Vpisi ime datoteke: ");
    gets(imedatoteke);
/*    printf("Vpisi stevilo vnosov: ");
    scanf("%d",&n);
	ZapisiCelaStevila(n,imedatoteke); */
    
    BeriCelaStevila(imedatoteke);
	return 0;
}

#include <stdio.h>

/*
 * Domača naloga:
 * naredite funkcijo, ki obrne vrstni red znakov v nizu.
 * Funkcijo implementirajte sami, klici funkcij v knjiznici string.h
 * niso dovoljeni.
 */
int main(int argc, char **argv)
{
	printf("hello world\n");
	return 0;
}

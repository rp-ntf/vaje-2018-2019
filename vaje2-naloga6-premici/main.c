#include <stdio.h>

int main(int argc, char **argv)
{
	/* Navodila : 
	 * Sestavi program, ki za dve premici 
	 * y = k1 * x + n1
	 * y = k2 * x + n2
	 * izpiše ali 
	 * - se sekata v eni točki (in izpiše točko)
	 * - se premici prekrivata 
	 * - sta premici vzporedni 
	 */
	int k1, k2, n1, n2;
	printf("Vpisite koeficiente premice 1 : ");
	scanf("%d %d", &k1, &n1 );
	printf("Premica 1 : y = %d * x + %d\n", k1, n1 );
	
	printf("Vpisite koeficiente premice 2 : ");
	scanf("%d %d", &k2, &n2 );
	printf("Premica 2 : y = %d * x + %d\n", k2, n2 );
    
    float Tx, Ty;
    if( k1 == k2 && n1 == n2 )
        printf("Premici se prekrivata\n");
    else if( k1 == k2 )
        printf("Premici sta vzporedni\n");
    else
    {
        Tx = ((float)(n1-n2))/(k2-k1);
        Ty = k1 * Tx + n1;
		printf("Premici se sekata v tocki T( %f, %f )\n", Tx, Ty);
    }
	return 0;
}

#include <stdio.h>
/* Napiši program, ki bo izpisal pravokotnik
 * sestavljen iz zvezdic (*) velikosti a X b.
 * */
 
int main()
{
	int i=0, j=0, a, b;
    
    printf("Vpisi dimenzijo v smeri x: "); 
    scanf("%d",&a);    
    printf("Vpisi dimenzijo v smeri y: "); 
    scanf("%d",&b);
    
    while (j<b) {
            i = 0;
            while (i<a) {
                printf("*");
                i++;                           // i=i+1;
            }
            printf("\n");
            j=j+1;                          // j++;
    }
    
    printf("\n");
	return 0;
}

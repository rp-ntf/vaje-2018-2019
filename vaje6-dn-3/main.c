
/* 3. Naloga:
* Sestavi program, ki prebere tabelo 10 števil in vrne
* indeks elementa katerega absolutna vrednost je najmanjša.
*/
#include <stdio.h>
#include <math.h>

int main()
{
	float tabela[10];
	int i_najmanjsi;
	float abs_najmanjsi;
	for( int i=0; i<10; ++i )
	{
		printf("Vnesite tabela[%d] : ", i );
		scanf("%f", &tabela[i]);
	}
	i_najmanjsi = 0;
	abs_najmanjsi = fabs(tabela[0]);
	for( int i=0; i<10; ++i )
	{
		if( fabs(tabela[i]) < abs_najmanjsi )
		{
			i_najmanjsi = i;
			abs_najmanjsi = fabs(tabela[i]);
		}
	}
	printf("Indeks elementa, najmanjsega po abs. vrednosti je %d (vrednost %f).\n",
			i_najmanjsi, abs_najmanjsi );
	return 0;
}
